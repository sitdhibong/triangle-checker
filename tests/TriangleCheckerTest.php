<?php

use PHPUnit\Framework\TestCase;

use Triangle\TriangleChecker;

class TriangleCheckerTest extends TestCase {

  public function testCheckerShouldReturnString() {
    $checker = new TriangleChecker();
    $triangleName = $checker->check(1, 1, 1);

    $this->assertIsString($triangleName);
  }

  /**
   * @dataProvider triangleDataProvider
   **/
  public function testTriangleCheck(int $a, int $b, int $c, string $expectedName): void {
    $checker = new TriangleChecker();

    $actualName = $checker->check($a, $b, $c);

    $this->assertEquals($expectedName, $actualName);
  }

  public function triangleDataProvider(): array {
    $iso = 'Isosceles Triangle';
    $sca = 'Scalene Triangle';
    $equ = 'Equilateral Triangle';
    $nat = 'Not a Triangle';

    return [
      [1, 1, 1, $equ],
      [4, 4, 3, $iso],
      [3, 4, 5, $sca],
      [0, 0, 0, $nat],
      // Add test data below

    ];
  }

}
