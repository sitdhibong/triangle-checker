<?php

namespace Triangle;

class TriangleChecker {
  protected $triangles = []; 

  public function __construct() {
    $this->triangles = [
      'iso' => 'Isosceles Triangle',
      'sca' => 'Scalene Triangle',
      'equ' => 'Equilateral Triangle',
      'nat' => 'Not a Triangle',
    ];
  }

  protected function sideLimit(int $side): int {
    return $side;
  }

  public function check(int $a, int $b, int $c): string {
    $a = min(202, $a);
    $b = max(0, $b);
    $c = min(200, max(1, $c));

    $label = $this->triangles['nat'];

    if (($a + $b > $c) && ($b + $c > $a) && ($c + $a > $b)) {
      if (($a == $b) && ($b == $c)) {
        $label = $this->triangles['equ'];
      } elseif (($a == $b) || ($b == $c) || ($c == $a)) {
        $label = $this->triangles['iso'];
      } else {
        $label = $this->triangles['sca'];
      }
    }

    return $label;
  }
}
